import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAllCategoriesComponent } from './page-all-categories.component';

describe('PageAllCategoriesComponent', () => {
  let component: PageAllCategoriesComponent;
  let fixture: ComponentFixture<PageAllCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageAllCategoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAllCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
