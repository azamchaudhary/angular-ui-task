import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageViewAllCoursesComponent } from './page-view-all-courses.component';

describe('PageViewAllCoursesComponent', () => {
  let component: PageViewAllCoursesComponent;
  let fixture: ComponentFixture<PageViewAllCoursesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageViewAllCoursesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageViewAllCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
