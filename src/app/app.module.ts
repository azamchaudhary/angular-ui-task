import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { PageCourseCategoryComponent } from './page-course-category/page-course-category.component';
import { PageAllCategoriesComponent } from './page-all-categories/page-all-categories.component';
import { PageCoursesComponent } from './page-courses/page-courses.component';
import { PageViewAllCoursesComponent } from './page-view-all-courses/page-view-all-courses.component';
import { PageBlogsComponent } from './page-blogs/page-blogs.component';
import { PageFooterComponent } from './page-footer/page-footer.component';
import { PageCarouselComponent } from './page-carousel/page-carousel.component';

@NgModule({
  declarations: [
    AppComponent,
    PageHeaderComponent,
    PageCourseCategoryComponent,
    PageAllCategoriesComponent,
    PageCoursesComponent,
    PageViewAllCoursesComponent,
    PageBlogsComponent,
    PageFooterComponent,
    PageCarouselComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
