import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageHeaderComponent } from './page-header/page-header.component';
import { PageCourseCategoryComponent } from './page-course-category/page-course-category.component';
import { PageAllCategoriesComponent } from './page-all-categories/page-all-categories.component';
import { PageCoursesComponent } from './page-courses/page-courses.component';
import { PageViewAllCoursesComponent } from './page-view-all-courses/page-view-all-courses.component';
import { PageBlogsComponent } from './page-blogs/page-blogs.component';
import { PageFooterComponent } from './page-footer/page-footer.component';


const routes: Routes = [ 
  {
    path: 'header',
    component:PageHeaderComponent,
  },
  
  {
    path: 'CourseCategory',
    component:PageCourseCategoryComponent,
  },
  {
    path: 'Allcategory',
    component:PageAllCategoriesComponent,
  },
  {
    path: 'Course',
    component:PageCoursesComponent,
  },
  {
    path: 'ViewAll',
    component:PageViewAllCoursesComponent,
  },
  {
    path: 'Blogs',
    component:PageBlogsComponent,
  },
  {
    path: 'Footer',
    component:PageFooterComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
