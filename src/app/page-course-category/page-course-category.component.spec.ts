import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCourseCategoryComponent } from './page-course-category.component';

describe('PageCourseCategoryComponent', () => {
  let component: PageCourseCategoryComponent;
  let fixture: ComponentFixture<PageCourseCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PageCourseCategoryComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCourseCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
