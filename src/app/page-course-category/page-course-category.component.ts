import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-page-course-category',
  templateUrl: './page-course-category.component.html',
  styleUrls: ['./page-course-category.component.scss']
})
export class PageCourseCategoryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $('.owl-carousel-page-course-category').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 5
        }
      }
    })
  }

}
